import typescript from "rollup-plugin-typescript"
import resolve from "rollup-plugin-node-resolve"

import pkg from "./package.json"

export default [
  {
    input: "src/main.ts",
    output: {
      name: "main.js",
      file: pkg.main,
      format: "iife"
    },
    plugins: [typescript(), resolve()],
    external: ["screeps-movement"]
  }
]
