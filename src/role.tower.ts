export const roleTower = tower => {
  var closestHostile = tower.pos.findClosestByRange(FIND_HOSTILE_CREEPS)
  if (closestHostile) {
    tower.attack(closestHostile)
    return true
  }
  let closestDamagedCreep = tower.pos.findClosestByRange(FIND_MY_CREEPS, {
    filter: creep => creep.hits < creep.hitsMax
  })
  if (closestDamagedCreep) {
    return tower.heal(closestDamagedCreep)
  }

  var closestDamagedStructure = tower.pos.findClosestByRange(FIND_STRUCTURES, {
    filter: structure => {
      if (
        structure.structureType == STRUCTURE_WALL ||
        structure.structureType == STRUCTURE_RAMPART
      ) {
        return structure.hits < 5000 && structure.hits < structure.hitsMax
      } else {
        return structure.hits < structure.hitsMax
      }
    }
  })

  if (!closestDamagedStructure) {
    closestDamagedStructure = tower.pos.findClosestByRange(FIND_STRUCTURES, {
      filter: structure => {
        if (
          structure.structureType == STRUCTURE_WALL ||
          structure.structureType == STRUCTURE_RAMPART
        ) {
          return structure.hits < 50000 && structure.hits < structure.hitsMax
        } else {
          return structure.hits < structure.hitsMax
        }
      }
    })
  }

  if (closestDamagedStructure) {
    tower.repair(closestDamagedStructure)
  }
}
