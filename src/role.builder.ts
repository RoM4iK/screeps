import { getResources } from "src/creep-utils"

export const roleBuilder = {
  run: function(creep: Creep) {
    if (creep.memory.storing === true && creep.carry.energy < 50) {
      creep.memory.repairTarget = undefined
      getResources(creep, false)
      if (creep.memory.withdrawTarget || creep.memory.pickupTarget) {
        return true
      }
      if (creep.pos.roomName != creep.memory.roomName) {
        creep.moveTo(new RoomPosition(25, 25, creep.memory.roomName), {
          avoidRooms: Memory.impassableRooms
        })
      }
      return true
    }

    creep.memory.withdrawTarget = undefined
    creep.memory.pickupTarget = undefined

    creep.memory.storing = creep.carry[RESOURCE_ENERGY] === 0
    if (creep.memory.storing) {
      return false
    }

    if (creep.memory.repairTarget) {
      let repairTarget: Structure = Game.getObjectById(
        creep.memory.repairTarget
      )
      if (repairTarget && repairTarget.hits < repairTarget.hitsMax) {
        if (repairTarget.hits > 5000) {
          creep.memory.repairTarget = undefined
          return false
        }
        let repairResult = creep.repair(repairTarget)
        if (repairResult == ERR_NOT_IN_RANGE) {
          creep.moveTo(repairTarget)
        }
        if (repairResult == OK) {
          return true
        }
      } else {
        creep.memory.repairTarget = undefined
      }
    }

    const possibleRepairTargets = creep.room.find(FIND_STRUCTURES, {
      filter: structure => {
        return (
          structure.hits < 3000 && structure.hits < structure.hitsMax * 0.75
        )
      }
    })

    possibleRepairTargets.sort((a, b) => a.hits - b.hits)

    if (possibleRepairTargets.length > 0) {
      let repairTarget = creep.pos.findClosestByPath(possibleRepairTargets)
      let repairResult = creep.repair(repairTarget)
      if (repairResult == ERR_NOT_IN_RANGE) {
        creep.moveTo(repairTarget)
      }
      if (repairResult == OK) {
        creep.memory.repairTarget = repairTarget.id
      }
      return true
    }

    if (
      _.get(creep, "room.controller.ticksToDowngrade") < 2500 &&
      _.get(creep, "room.controller.my")
    ) {
      if (creep.upgradeController(creep.room.controller) == ERR_NOT_IN_RANGE) {
        creep.moveTo(creep.room.controller, {
          visualizePathStyle: { stroke: "#ffffff" }
        })
      }
    }

    let target

    let constructionSites = _.flatten(
      _.map(Game.ownedRooms, room => room.find(FIND_MY_CONSTRUCTION_SITES))
    )

    target = creep.pos.findClosestByPath(constructionSites, {
      filter: cs => cs.structureType == STRUCTURE_SPAWN
    })

    if (!target) {
      target = creep.pos.findClosestByPath(constructionSites, {
        filter: cs =>
          cs.structureType == STRUCTURE_CONTAINER ||
          cs.structureType == STRUCTURE_TOWER
      })
    }

    if (!target) {
      target = creep.pos.findClosestByPath(constructionSites, {
        filter: cs => cs.structureType == STRUCTURE_EXTENSION
      })
    }

    if (!target) {
      target = creep.pos.findClosestByPath(constructionSites, {
        filter: cs => cs.structureType == STRUCTURE_ROAD
      })
    }

    if (!target) {
      target = creep.pos.findClosestByPath(constructionSites)
    }

    if (!target) {
      target = _.first(constructionSites)
    }

    if (target) {
      if (creep.build(target) == ERR_NOT_IN_RANGE) {
        creep.moveTo(target, {
          visualizePathStyle: { stroke: "#ffffff" }
        })
      }
    }
    return true
  }
}
