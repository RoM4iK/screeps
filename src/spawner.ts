import { registerFN } from "src/screeps-profiler"

import {
  ROLE_ATTACKER,
  ROLE_BUILDER,
  ROLE_HARVESTER,
  ROLE_UPGRADER,
  ROLE_COURIER,
  ROLE_DEMOLISHER,
  ROLE_SCOUT,
  ROLE_EXTERNAL_HARVESTER,
  ROLE_MINERAL_HARVESTER,
  ROLE_CLAIMER,
  PART_COSTS
} from "src/constants"

const COURIER_PATTERN = [CARRY, CARRY, MOVE]
const COURIER_PATTERN_SIZE = PART_COSTS[CARRY] * 2 + PART_COSTS[MOVE]
const COURIER_LEFT_PATTERN = {
  0: [],
  50: [CARRY],
  100: [CARRY, CARRY]
}

const buildCourierBody = capacity => {
  capacity = capacity > 750 ? 750 : capacity
  let body = []
  let patternSize = Math.floor(capacity / COURIER_PATTERN_SIZE)
  let sizeLeft = capacity % COURIER_PATTERN_SIZE
  sizeLeft = Math.floor(sizeLeft / 50) * 50
  _.times(patternSize, () => (body = body.concat(COURIER_PATTERN)))
  body = body.concat(COURIER_LEFT_PATTERN[sizeLeft])
  return body
}

const buildHarvesterBody = (capacity, haveHarvestLink = false) => {
  let body = []
  let bodyWeight = 0
  let maxWorkCapacity = haveHarvestLink ? 600 : 550
  let workPartsCount =
    capacity < maxWorkCapacity
      ? (capacity - PART_COSTS[MOVE]) / PART_COSTS[WORK]
      : 5
  _.times(workPartsCount, () => body.push(WORK))
  bodyWeight = workPartsCount * PART_COSTS[WORK]

  let moveParts = (capacity - bodyWeight) / 50
  if (moveParts > 3) {
    moveParts = 3
  }
  _.times(moveParts, () => body.push(MOVE))
  bodyWeight += moveParts * PART_COSTS[MOVE]
  if (haveHarvestLink) {
    body.push(CARRY)
    bodyWeight += 50
  }
  return body
}

const buildMineralHarvesterBody = capacity => {
  capacity = capacity > 2000 ? 2000 : capacity
  let body = []
  let patternsCount = Math.floor(capacity / 250)
  let workPartsCount = patternsCount * 2
  let movePartsCount = workPartsCount / 2
  _.times(workPartsCount, () => body.push(WORK))
  _.times(movePartsCount, () => body.push(MOVE))
  return body
}

const buildExternalHarvesterBody = capacity => {
  capacity = capacity > 2000 ? 2000 : capacity
  let body = []
  let workPartsCount = Math.floor(capacity / 250)
  let movePartsCount = workPartsCount * 2
  _.times(workPartsCount, () => body.push(WORK))
  _.times(workPartsCount, () => body.push(CARRY))
  _.times(movePartsCount, () => body.push(MOVE))
  return body
}

const buildUpgraderBody = (capacity, room: Room) => {
  capacity = capacity > 1800 ? 1750 : capacity - 50
  if (capacity < 350) {
    return [WORK, WORK, CARRY, MOVE]
  }
  if (_.lt(room.roomStore[RESOURCE_ENERGY], 10000)) {
    capacity = 400
  }
  let body = []
  let patternsCount = Math.floor(capacity / 350)
  let workPartsCount = patternsCount * 3
  _.times(workPartsCount, () => body.push(WORK))
  body.push(CARRY)
  _.times(patternsCount, () => body.push(MOVE))
  return body
}

const BUILDER_PATTERN = [WORK, CARRY, MOVE]
const BUILDER_PATTERN_SIZE = 200
const BUILDER_LEFT_PATTERN = {
  0: [],
  50: [CARRY],
  100: [CARRY, MOVE],
  150: [CARRY, CARRY, MOVE]
}

const buildBuilderBody = capacity => {
  capacity = capacity > 3200 ? 3200 : capacity
  let body = []
  let patternSize = Math.floor(capacity / BUILDER_PATTERN_SIZE)
  let sizeLeft = capacity % BUILDER_PATTERN_SIZE
  sizeLeft = Math.floor(sizeLeft / 50) * 50
  _.times(patternSize, () => (body = body.concat(BUILDER_PATTERN)))
  body = body.concat(BUILDER_LEFT_PATTERN[sizeLeft])
  return body
}

const buildDemolisherBody = capacity => {
  let body = []
  let patternsCount = Math.floor(capacity / 460)
  if (patternsCount > 10) {
    patternsCount = 10
  }
  _.times(patternsCount, () => body.push(TOUGH))
  _.times(patternsCount, () => body.push(MOVE))
  _.times(patternsCount, () => body.push(MOVE))
  _.times(patternsCount, () => body.push(WORK))
  _.times(patternsCount, () => body.push(HEAL))
  return body
}

const buildAttackerBody = capacity => {
  let body = []
  let patternsCount = Math.floor(capacity / 650)
  _.times(patternsCount, () => body.push(MOVE))
  _.times(patternsCount, () => body.push(CLAIM))
  return body
}

const _processSpawn = (spawn: StructureSpawn) => {
  if (!spawn) {
    return
  }
  if (spawn.spawning) {
    var spawningCreep = Game.creeps[spawn.spawning.name]
    spawn.room.visual.text(
      "🛠️" + spawningCreep.memory.role,
      spawn.pos.x + 1,
      spawn.pos.y,
      { align: "left", opacity: 0.8 }
    )
    return true
  } else {
    let {
      creeps,
      energyCapacityAvailable,
      energyAvailable,
      memory: { spawnCreepsCount },
      name: roomName
    } = spawn.room
    let minimumSpawnEnergy = energyAvailable < 300 ? 300 : energyAvailable
    let groupedCreeps = _.groupBy(creeps, "memory.role")

    while (!spawn.spawning) {
      if (_.size(groupedCreeps[ROLE_COURIER]) < 1) {
        var newName = "[C]" + Game.time
        spawn.spawnCreep(buildCourierBody(minimumSpawnEnergy), newName, {
          memory: { role: ROLE_COURIER, roomName }
        })
        break
      }

      if (_.size(groupedCreeps[ROLE_HARVESTER]) < 1) {
        var newName = "[H]" + Game.time
        spawn.spawnCreep(
          buildHarvesterBody(
            minimumSpawnEnergy,
            spawn.room.harvestLinks.length > 0
          ),
          newName,
          {
            memory: { role: ROLE_HARVESTER, roomName }
          }
        )
        break
      }

      if (
        _.size(groupedCreeps[ROLE_HARVESTER]) < spawnCreepsCount.harvestersCount
      ) {
        var newName = "[H]" + Game.time
        spawn.spawnCreep(
          buildHarvesterBody(
            energyCapacityAvailable,
            spawn.room.harvestLinks.length > 0
          ),
          newName,
          {
            memory: { role: ROLE_HARVESTER, roomName }
          }
        )
        break
      }

      if (
        _.size(groupedCreeps[ROLE_UPGRADER]) < spawnCreepsCount.upgradersCount
      ) {
        var newName = "[U]" + Game.time
        spawn.spawnCreep(
          buildUpgraderBody(energyCapacityAvailable, spawn.room),
          newName,
          {
            memory: { role: ROLE_UPGRADER, roomName }
          }
        )
        break
      }

      if (
        _.size(groupedCreeps[ROLE_COURIER]) < spawnCreepsCount.couriersCount
      ) {
        var newName = "[C]" + Game.time
        spawn.spawnCreep(buildCourierBody(energyCapacityAvailable), newName, {
          memory: { role: ROLE_COURIER, roomName }
        })
        break
      }

      if (
        _.size(groupedCreeps[ROLE_BUILDER]) < spawnCreepsCount.buildersCount
      ) {
        var newName = "[B]" + Game.time
        spawn.spawnCreep(buildBuilderBody(energyCapacityAvailable), newName, {
          memory: { role: ROLE_BUILDER, roomName }
        })
        break
      }

      if (_.size(groupedCreeps[ROLE_SCOUT]) < spawnCreepsCount.scoutsCount) {
        var newName = "[S]" + Game.time
        spawn.spawnCreep([TOUGH, MOVE], newName, {
          memory: { role: ROLE_SCOUT, roomName }
        })
        break
      }

      if (
        _.size(groupedCreeps[ROLE_DEMOLISHER]) <
        spawnCreepsCount.demolishersCount
      ) {
        var newName = "[D]" + Game.time
        spawn.spawnCreep(
          buildDemolisherBody(energyCapacityAvailable),
          newName,
          {
            memory: { role: ROLE_DEMOLISHER, roomName }
          }
        )
        break
      }

      if (
        _.size(groupedCreeps[ROLE_EXTERNAL_HARVESTER]) <
        spawnCreepsCount.externalHarvestersCount
      ) {
        var newName = "[EH]" + Game.time
        spawn.spawnCreep(
          buildExternalHarvesterBody(energyCapacityAvailable),
          newName,
          {
            memory: {
              role: ROLE_EXTERNAL_HARVESTER,
              roomName
            }
          }
        )
        break
      }

      if (
        _.size(groupedCreeps[ROLE_CLAIMER]) < spawnCreepsCount.claimersCount
      ) {
        var newName = "[CL]" + Game.time
        spawn.spawnCreep([CLAIM, MOVE, MOVE, MOVE, MOVE, MOVE], newName, {
          memory: {
            role: ROLE_CLAIMER,
            roomName
          }
        })
        break
      }

      if (
        _.size(groupedCreeps[ROLE_MINERAL_HARVESTER]) <
        spawnCreepsCount.mineralHarvestersCount
      ) {
        var newName = "[MH]" + Game.time
        spawn.spawnCreep(
          buildMineralHarvesterBody(energyCapacityAvailable),
          newName,
          {
            memory: {
              role: ROLE_MINERAL_HARVESTER,
              roomName
            }
          }
        )
        break
      }

      if (
        _.size(groupedCreeps[ROLE_ATTACKER]) < spawnCreepsCount.attackersCount
      ) {
        var newName = "[A]" + Game.time
        spawn.spawnCreep(buildAttackerBody(energyCapacityAvailable), newName, {
          memory: {
            role: ROLE_ATTACKER,
            roomName
          }
        })
        break
      }

      break
    }
  }
}

export const processSpawn: (spawn: StructureSpawn) => void = registerFN(
  _processSpawn,
  "spawner.processSpawn"
)
