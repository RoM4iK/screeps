import { registerFN } from "src/screeps-profiler"

const _createEnergyBuyOrders = (orders: Order[], ownOrders: Order[]) => {
  let maxBuyPrice = 0.05

  const marketPrice: any = _(orders)
    .filter(order => {
      return _.gte(order.amount, 10000)
    })
    .pluck("price")
    .max()

  const myPrice = marketPrice + 0.001
  const groupedOwnOrders = _.groupBy(ownOrders, "roomName")
  const priceHigherThanMax = _.gt(myPrice, maxBuyPrice)
  if (priceHigherThanMax) {
    return
  }
  const buyAmount = 10000

  _.each(Game.ownedRoomsWithTerminal, room => {
    const order = _.first(groupedOwnOrders[room.name])

    if (order) {
      if (order.price != myPrice) {
        Game.market.changeOrderPrice(order.id, myPrice)
      }
      if (_.lt(order.remainingAmount, buyAmount)) {
        Game.market.extendOrder(order.id, buyAmount - order.remainingAmount)
      }

      return true
    }

    Game.market.createOrder(
      ORDER_BUY,
      RESOURCE_ENERGY,
      myPrice,
      buyAmount,
      room.name
    )
  })
}

// @ts-ignore
export const createEnergyBuyOrders: (
  orders: Order[],
  ownOrders: Order[]
) => void = registerFN(_createEnergyBuyOrders, "market.createEnergyBuyOrders")

const _dealMinedMineral = (mineral: MineralConstant, orders: Order[]) => {
  if (Game.globalStore[mineral] < 150000) {
    return false
  }

  _.some(orders, order => {
    if (order.price < 0.5) {
      return
    }

    return _.some(Game.ownedRoomsByMineral[mineral], (room: Room) => {
      if (!room.terminal) {
        return
      }
      if (Game.map.getRoomLinearDistance(room.name, order.roomName) > 15) {
        return
      }

      const amount = _.min([order.amount, room.terminal.store[mineral]])
      if (amount === 0) {
        return
      }

      Game.market.deal(order.id, amount, room.name)
      return true
    })
  })
}

// @ts-ignore
export const dealMinedMineral: (
  mineral: MineralConstant,
  orders: Order[]
) => void = registerFN(_dealMinedMineral, "market.dealMinedMineral")

const _processMarket = () => {
  const ownedRoomNames = _.pluck(Game.ownedRooms, "name")
  const sellableMinerals: MarketResourceConstant[] = [
    RESOURCE_KEANIUM,
    RESOURCE_LEMERGIUM,
    RESOURCE_UTRIUM,
    RESOURCE_ZYNTHIUM
  ]
  const orders = Game.market.getAllOrders(
    order => !ownedRoomNames.includes(order.roomName)
  )
  const groupedOrders = _.groupBy(orders, "type")
  const groupedBuyOrders = _.groupBy(groupedOrders[ORDER_BUY], "resourceType")
  let ownOrders: Order[] = _.values(Game.market.orders)
  ownOrders = _.filter(ownOrders, order => {
    if (order.remainingAmount == 0) {
      Game.market.cancelOrder(order.id)
      return false
    }
    return true
  })
  let groupedOwnOrders = _.groupBy(ownOrders, "type")
  const groupedOwnBuyOrders = _.groupBy(
    groupedOwnOrders[ORDER_BUY],
    "resourceType"
  )

  if (
    _.lt(Game.globalStore[RESOURCE_ENERGY], Game.ownedRooms.length * 100000)
  ) {
    createEnergyBuyOrders(
      groupedBuyOrders[RESOURCE_ENERGY],
      groupedOwnBuyOrders[RESOURCE_ENERGY]
    )
  }

  _.each(groupedBuyOrders, (v, k: MineralConstant) => {
    // const marketPrice = _(v)
    //   .map("price")
    //   .max()

    // const timeKey = Math.floor(Game.time / 1000)
    // const objectKey = `market.rawHistory.${k}.buy.t${timeKey}`
    //
    // const prices = _.get(Memory, objectKey, [])
    // _.set(Memory, objectKey, prices.concat(marketPrice))

    const isMined = Game.minedMinerals.includes(k)
    const isSellable = sellableMinerals.includes(k)
    if (isMined && isSellable) {
      dealMinedMineral(k, v)
    }
  })
}
// @ts-ignore
export const processMarket: () => void = registerFN(
  _processMarket,
  "market.processMarket"
)
