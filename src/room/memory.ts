import { registerFN } from "src/screeps-profiler"

const _writeNotOwnedMemory = (room: Room) => {
  let { controller, memory } = room
  let owner = _.get(controller, "owner.username") || null

  let creepsCount = room.find(FIND_HOSTILE_CREEPS).length
  let sourcesCount = memory.sourcesCount || room.find(FIND_SOURCES).length

  _.assign(memory, {
    sourcesCount,
    owner,
    creepsCount,
    name: room.name,
    towersCount: room.towers.length,
    spawnsCount: room.spawns.length,
    controller: _.pick(room.controller, [
      "ticksToDowngrade",
      "level",
      "safeModeAvailable",
      "safeMode",
      "upgradeBlocked",
      "my"
    ]),
    updatedAt: Game.time
  })
}

export const writeNotOwnedMemory = registerFN(
  _writeNotOwnedMemory,
  "room.writeNotOwnedMemory"
)

const _writeOwnedMemory = (room: Room) => {
  let { memory } = room
  let couriersCount = 2
  let upgradersCount = 1
  let harvestersCount = 2
  let buildersCount = 0
  let scoutsCount = 0
  let demolishersCount = 0
  let externalHarvestersCount = 0
  let mineralHarvestersCount = 0
  let claimersCount = 0
  let attackersCount = 0

  let hostileCreepsCount = room.find(FIND_HOSTILE_CREEPS).length

  if (room.constructionSites.length > 0) {
    buildersCount = 1
  }

  _.each(room.claimFlags, (flag: Flag) => {
    let memory = Memory.rooms[flag.pos.roomName]
    let room = Game.rooms[flag.pos.roomName]
    if (
      !_.get(room, "controller.my") &&
      (!memory || memory.updatedAt + 100 < Game.time)
    ) {
      scoutsCount++
      return
    }

    if (_.get(room, "controller.my")) {
      let buildingSpawn = room.find(FIND_MY_CONSTRUCTION_SITES, {
        filter: cs => cs.structureType == STRUCTURE_SPAWN
      })

      if (_.size(buildingSpawn) > 0) {
        buildersCount++
      }
      return
    }

    if (memory.owner === null && Game.ownedRooms.length < Game.gcl.level) {
      claimersCount++
      return
    }

    if (memory.towersCount > 0) {
      demolishersCount = 1 + memory.towersCount
    } else {
      if (memory.spawnsCount > 0) {
        demolishersCount = 1
      }
    }
  })

  _.each(room.reserveFlags, (flag: Flag) => {
    let memory = Memory.rooms[flag.pos.roomName]
    if (
      !memory ||
      (!_.get(memory, "controller.my") && memory.updatedAt + 100 < Game.time)
    ) {
      scoutsCount++
      return
    }

    let haveCreeps = memory.creepsCount > 0
    let haveTowers = memory.towersCount > 0

    if (!haveCreeps && !haveTowers && memory.owner == null) {
      externalHarvestersCount += memory.sourcesCount
    }

    if (memory.towersCount > 0) {
      demolishersCount = 1 + memory.towersCount
    } else {
      if (memory.spawnsCount > 0) {
        demolishersCount = 1
      }
    }
  })

  const harvestableMinerals = room.find(FIND_MINERALS, {
    filter: mineral => mineral.mineralAmount > 0
  })

  if (room.mineralFlags.length > 0 && harvestableMinerals.length > 0) {
    mineralHarvestersCount++
  }

  if (
    demolishersCount == 0 &&
    room.storage &&
    _.gt(room.roomStore[RESOURCE_ENERGY], 50000) &&
    room.controller.level < 8
  ) {
    upgradersCount = 3
  }

  if (!room.storage && buildersCount == 0) {
    upgradersCount = 3
  }

  if (room.storageLinks || room.controllerLink) {
    couriersCount = 1
  }

  if (hostileCreepsCount > 0) {
    couriersCount < 2 && couriersCount++
  }

  if (room.terminal && _.sum(room.terminal.store) > 295000) {
    couriersCount++
  }

  if (
    _(room.find(FIND_DROPPED_RESOURCES))
      .filter(r => r.resourceType == RESOURCE_ENERGY)
      .sum("amount") > 1000
  ) {
    couriersCount++
  }

  _.assign(memory, {
    hostileCreepsCount,
    spawnCreepsCount: {
      couriersCount,
      upgradersCount,
      harvestersCount,
      buildersCount,
      scoutsCount,
      demolishersCount,
      externalHarvestersCount,
      mineralHarvestersCount,
      claimersCount,
      attackersCount
    },
    updatedAt: Game.time
  })
}

export const writeOwnedMemory = registerFN(
  _writeOwnedMemory,
  "room.writeOwnedMemory"
)
