import { registerFN } from "src/screeps-profiler"

import { processSpawn } from "src/spawner"
import { roleTower } from "src/role.tower"
import { processLink } from "src/link"
import { processLab } from "src/lab"
import { processObserver } from "src/observer"
import { writeOwnedMemory, writeNotOwnedMemory } from "src/room/memory"
import { manageReaction } from "src/room/reaction"
import { shareEnergy } from "src/room/shareEnergy"

import {
  createHarvestFlags,
  createMineralFlags,
  createUpgradeFlags
} from "src/flags"

const _processRoom = (room: Room, time: number) => {
  let { controller, memory } = room

  if (_.get(controller, "my")) {
    _.forEach(room.towers, roleTower)

    if (time % 5 == 0) {
      _.forEach(room.labs, processLab)
    }
    if (!memory.notOwnedStructuresCleaned) {
      const notOwnedStructures = _.filter(room.structures, (s: any) => {
        return s.owner && !s.my
      })
      memory.notOwnedStructuresCleaned = !notOwnedStructures.length
      _(notOwnedStructures)
        .filter((s: any) => _.get(s, "store.energy", 0) < 100)
        .each(s => {
          s.destroy()
        })
        .value()
    }
  }

  if (time % 100 == 0) {
    memory.haveFlags = false
  }

  if (time % 1000 == 0) {
    memory.reaction = ""
  }

  if (!memory.haveFlags) {
    createHarvestFlags(room)
    createMineralFlags(room)
    createUpgradeFlags(room)
  }

  if (
    _.lt(Game.cpu.bucket, 1000) ||
    _.gt(_.get(memory, "updatedAt", 0) + 20, time)
  ) {
    return true
  }

  if (!memory.mineral) {
    memory.mineral = room.find(FIND_MINERALS)[0].mineralType
  }

  if (_.get(controller, "my")) {
    _.forEach(room.spawns, processSpawn)
    _.forEach(room.links, processLink)
    if (room.terminal) {
      shareEnergy(room)
      manageReaction(room)
    }

    if (room.observer) {
      processObserver(room.observer)
    }
    writeOwnedMemory(room)
  } else {
    writeNotOwnedMemory(room)
  }
}

export const processRoom = registerFN(_processRoom, "room.processRoom")
