import { registerFN } from "src/screeps-profiler"
import { REACTION_TYPES, REACTION_TARGETS } from "src/constants"

const reactionIsPossible = (room, resource, limit = null) => {
  if (limit && _.get(Game.globalStore, resource, 0) > limit) {
    return false
  }
  const reaction = REACTION_TYPES[resource]
  if (_.isEmpty(reaction)) {
    return false
  }
  return _.every(reaction, (reagent: ResourceConstant) => {
    const haveLocalResources = _.gt(room.terminal.store[reagent], 1500)
    const haveGlobalResources = () =>
      _.some(Game.ownedRoomsWithTerminal, r => canTransferReagent(r, reagent))
    return haveLocalResources || haveGlobalResources()
  })
}

const canTransferReagent = (room: Room, reagent: ResourceConstant) => {
  const storedAmount = _.get(room.terminal.store, reagent, 0)
  if (room.reactionType.includes(reagent) && storedAmount < 6000) {
    return
  }
  return storedAmount > 1500
}

const getPossibleReaction = (room, resource, limit = 30000) => {
  if (_.get(Game.globalStore, resource, 0) > limit) {
    return
  }
  if (reactionIsPossible(room, resource, limit)) {
    return resource
  }
  const reaction = REACTION_TYPES[resource]
  let targetReaction = ""
  _.some(reaction, reagent => {
    targetReaction = getPossibleReaction(room, reagent, limit)
    return !!targetReaction
  })
  return targetReaction
}

const _manageReaction = (room: Room) => {
  if (!room.terminal) {
    return
  }
  if (!reactionIsPossible(room, room.memory.reaction)) {
    room.memory.reaction = ""
  }
  if (!room.memory.reaction) {
    if (room.controller.level < 8) {
      if (reactionIsPossible(room, "XGH2O")) {
        room.memory.reaction = "XGH2O"
        console.log(`Started XGH2O production`, room)
        return
      }
    }
    const haveReaction = _.some(REACTION_TARGETS, (limit, resource) => {
      const possibleReaction = getPossibleReaction(room, resource, limit)
      if (possibleReaction) {
        console.log(`Started ${possibleReaction} production`, room)
        room.memory.reaction = possibleReaction
        return true
      }
    })
    if (!haveReaction) {
      if (room.memory.debug) {
        console.log("Room doesn't have a reaction", room)
      }
    }
    return
  }

  if (Game.globalStore[room.reaction] > REACTION_TARGETS[room.reaction]) {
    room.memory.reaction = ""
  }

  _.some(room.reactionType, reagent => {
    const localStoredAmount = _.get(room.terminal.store, reagent, 0)
    if (localStoredAmount > 3000) {
      return
    }

    const resourceTransfered = _.some(
      Game.ownedRoomsWithTerminal,
      transferableRoom => {
        if (!canTransferReagent(transferableRoom, reagent)) {
          return
        }
        if (transferableRoom.terminal.cooldown > 0) {
          return true
        }
        const storedAmount = _.get(transferableRoom.terminal.store, reagent, 0)

        let amount = _.min([storedAmount, 3000])
        transferableRoom.terminal.send(reagent, amount, room.name)
        return true
      }
    )
    if (resourceTransfered || localStoredAmount > 500) {
      return
    }

    room.memory.reaction = ""
    return true
  })
}

export const manageReaction = registerFN(_manageReaction, "room.manageReaction")
