import { registerFN } from "src/screeps-profiler"

const _shareEnergy = (room: Room) => {
  const { controller, terminal } = room
  if (
    controller.level < 8 ||
    !terminal ||
    room.roomStore[RESOURCE_ENERGY] < 100000 ||
    terminal.store[RESOURCE_ENERGY] < 10000
  ) {
    return
  }

  const transferTarget = _(Game.ownedRoomsWithTerminal)
    .filter(room => room.roomStore[RESOURCE_ENERGY] < 100000)
    .sortBy(room => room.roomStore[RESOURCE_ENERGY])
    .first()

  if (!transferTarget || transferTarget.name == room.name) {
    return
  }

  terminal.send(
    RESOURCE_ENERGY,
    room.terminal.store[RESOURCE_ENERGY] - 5000,
    transferTarget.name
  )
}

export const shareEnergy = registerFN(_shareEnergy, "room.shareEnergy")
