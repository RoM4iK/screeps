import { roleAttacker } from "src/role.attacker"
import { roleBuilder } from "src/role.builder"
import { roleUpgrader } from "src/role.upgrader"
import { roleCourier } from "src/role.courier"
import { roleDemolisher } from "src/role.demolisher"
import { roleExternalHarvester } from "src/role.external-harvester"
import { roleMineralHarvester } from "src/role.mineral-harvester"
import { roleClaimer } from "src/role.claimer"
import { roleScout } from "src/role.scout"

import { createCachedGetter } from "src/utils"
import { processHarvester } from "src/harvester"
import { processRoom } from "src/room"
import { processMarket } from "src/market"
import { createDefinitions } from "src/createDefinitions"

import profiler from "src/screeps-profiler"
import "screeps-movement"

import {
  ROLE_ATTACKER,
  ROLE_BUILDER,
  ROLE_HARVESTER,
  ROLE_UPGRADER,
  ROLE_COURIER,
  ROLE_SCOUT,
  ROLE_DEMOLISHER,
  ROLE_EXTERNAL_HARVESTER,
  ROLE_MINERAL_HARVESTER,
  ROLE_CLAIMER
} from "src/constants"

declare global {
  interface CreepMemory {
    [key: string]: any
  }

  interface RoomMemory {
    [key: string]: any
  }

  interface FlagMemory {
    [key: string]: any
  }

  interface Game {
    ownedRooms: Room[]
    ownedRoomsWithTerminal: Room[]
    ownedRoomsByMineral: Map<MineralConstant, Room[]>
    minedMinerals: MineralConstant[]
    globalStore: StoreDefinition
  }

  interface Creep {
    disableNotifications: () => void
  }
}

const creepActions = {
  [ROLE_BUILDER]: roleBuilder.run,
  [ROLE_HARVESTER]: processHarvester,
  [ROLE_UPGRADER]: roleUpgrader.run,
  [ROLE_COURIER]: roleCourier.run,
  [ROLE_SCOUT]: roleScout,
  [ROLE_DEMOLISHER]: roleDemolisher,
  [ROLE_EXTERNAL_HARVESTER]: roleExternalHarvester,
  [ROLE_MINERAL_HARVESTER]: roleMineralHarvester,
  [ROLE_CLAIMER]: roleClaimer,
  [ROLE_ATTACKER]: roleAttacker
}

profiler.enable()

Creep.prototype.disableNotifications = function() {
  if (this.memory.notifications === false) return OK
  const result = this.notifyWhenAttacked(false)
  if (result === OK) this.memory.notifications = false
  return result
}

createDefinitions()

let processGame = function() {
  const time = Game.time
  _.each(_.values(Game.creeps), (creep: Creep) => {
    creepActions[creep.memory.role](creep)
  })

  _.each(Game.rooms, room => {
    processRoom(room, time)
  })

  if (time % 20 == 0) {
    processMarket()
  }

  if (time % 500 == 0) {
    for (let name in Memory.creeps) {
      if (!Game.creeps[name]) {
        delete Memory.creeps[name]
      }
    }

    for (let name in Memory.flags) {
      if (!Game.flags[name]) {
        delete Memory.flags[name]
      }
    }

    for (let id in Memory.links) {
      let link: StructureLink = Game.getObjectById(id)
      if (!link || link.structureType != STRUCTURE_LINK) {
        delete Memory.links[id]
      }
    }

    for (let id in Memory.labs) {
      let lab: StructureLab = Game.getObjectById(id)
      if (!lab || lab.structureType != STRUCTURE_LAB) {
        delete Memory.labs[id]
      }
    }

    _.each(Memory.rooms, (roomMemory, name) => {
      if (_.gt(time - 100, roomMemory.updatedAt)) {
        delete Memory.rooms[name]
      }
    })
  }
}

const loop = function() {
  createCachedGetter(Game, "ownedRooms", function() {
    return _(Game.rooms)
      .values()
      .filter(room => _.get(room, "controller.my"))
      .value()
  })

  createCachedGetter(Game, "ownedRoomsWithTerminal", function() {
    return _.filter(Game.ownedRooms, room => !!room.terminal)
  })

  createCachedGetter(Game, "ownedRoomsByMineral", function() {
    return _.groupBy(Game.ownedRooms, room => room.memory.mineral)
  })

  createCachedGetter(Game, "minedMinerals", function() {
    return _.map(Game.ownedRooms, room => room.memory.mineral)
  })

  createCachedGetter(Game, "globalStore", function() {
    return _(Game.ownedRooms)
      .map(room => room.roomStore)
      .reduce((result, store) => {
        _.each(store, (v, k) => {
          result[k] = _.sum([v, result[k]])
        })
        return result
      }, {})
  })

  createCachedGetter(Game, "activeReactions", function() {
    return _(Game.ownedRooms)
      .map(room => room.reaction)
      .compact()
      .groupBy()
  })

  profiler.wrap(processGame)
}

module.exports.loop = loop
