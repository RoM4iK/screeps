import { FLAG_HARVEST } from "src/constants"

export const roleExternalHarvester = (creep: Creep) => {
  let { deliveryTargetId } = creep.memory
  if (
    !deliveryTargetId &&
    creep.carry[RESOURCE_ENERGY] == creep.carryCapacity
  ) {
    deliveryTargetId = Game.rooms[creep.memory.roomName].storage.id
  }
  if (deliveryTargetId) {
    const deliveryTarget: StructureStorage = Game.getObjectById(
      deliveryTargetId
    )
    if (!deliveryTarget || creep.carry[RESOURCE_ENERGY] === 0) {
      creep.memory.deliveryTargetId = undefined
      return false
    }
    if (creep.pos.roomName != deliveryTarget.pos.roomName) {
      const targetRoomPosition = new RoomPosition(
        25,
        25,
        deliveryTarget.pos.roomName
      )
      creep.moveTo(targetRoomPosition, {
        visualizePathStyle: { stroke: "#ffffff" }
      })
      return true
    }
    let transferResult = creep.transfer(deliveryTarget, RESOURCE_ENERGY)
    if (transferResult == ERR_NOT_IN_RANGE) {
      creep.moveTo(deliveryTarget, {
        visualizePathStyle: { stroke: "#ffffff" }
      })
      return true
    }
    if (transferResult == OK) {
      creep.memory.deliveryTarget = null
      return true
    }
  }

  if (creep.memory.targetFlag) {
    const flag = Game.flags[creep.memory.targetFlag]
    if (!flag || !flag.memory.sourceId) {
      creep.memory.targetFlag = undefined
      return false
    }
    const source: Source = Game.getObjectById(flag.memory.sourceId)
    const result = creep.harvest(source)
    if (creep.pos != flag.pos) {
      creep.moveTo(flag, {
        visualizePathStyle: { stroke: "#ffaa00" }
      })
      return true
    }
    if (result == OK || result == ERR_INVALID_TARGET || result == ERR_BUSY) {
      return true
    }
    console.log(result, creep)
    creep.say("Lazy ⛏")
  }

  const homeRoom = Game.rooms[creep.memory.roomName]

  _.each(homeRoom.reserveFlags, reserveFlag => {
    let harvestFlags = _.filter(
      Game.flags,
      (f: Flag) =>
        f.memory.type == FLAG_HARVEST &&
        f.pos.roomName == reserveFlag.pos.roomName &&
        f.memory.roomName == creep.memory.roomName
    )
    _.some(harvestFlags, flag => {
      const haveMiner = _.some(
        _.values(Game.creeps),
        (miner: Creep) =>
          miner.memory.role == creep.memory.role &&
          miner.memory.targetFlag == flag.name
      )

      if (!haveMiner) {
        creep.memory.targetFlag = flag.name
        return true
      }
    })
  })

  return false
}
