import { getResources } from "src/creep-utils"
import { processBoosting } from "src/creep-utils"

const UPGRADER_BOOSTING = {
  work: "XGH2O"
}

export const roleUpgrader = {
  run: function(creep) {
    let isBoosting = processBoosting(creep, UPGRADER_BOOSTING)
    if (isBoosting) {
      return true
    }
    const workPartsCount = _(creep.body)
      .filter(part => part.type == WORK)
      .size()
    if (creep.carry[RESOURCE_ENERGY] < workPartsCount) {
      if (
        creep.room.controllerLink &&
        creep.room.controllerLink.energy >= creep.carryCapacity
      ) {
        let withdrawResult = creep.withdraw(
          creep.room.controllerLink,
          RESOURCE_ENERGY
        )
        if (withdrawResult == ERR_NOT_IN_RANGE) {
          creep.moveTo(creep.room.controllerLink, {
            visualizePathStyle: { stroke: "#ffaa00" }
          })
          return true
        }
      } else {
        let containers = creep.room.controller.pos.findInRange(
          FIND_STRUCTURES,
          2,
          {
            filter: structure =>
              structure.structureType == STRUCTURE_CONTAINER &&
              structure.store[RESOURCE_ENERGY] > 0
          }
        )
        let closestContainer = creep.pos.findClosestByPath(containers)
        if (closestContainer) {
          let withdrawResult = creep.withdraw(closestContainer, RESOURCE_ENERGY)
          if (withdrawResult == ERR_NOT_IN_RANGE) {
            creep.moveTo(closestContainer, {
              visualizePathStyle: { stroke: "#ffaa00" }
            })
            return true
          }
        } else {
          getResources(creep, false)
          return
        }
      }
    }

    creep.memory.withdrawTarget = undefined
    creep.memory.pickupTarget = undefined
    if (creep.upgradeController(creep.room.controller) == ERR_NOT_IN_RANGE) {
      creep.moveTo(creep.room.controller, {
        visualizePathStyle: { stroke: "#ffffff" }
      })
    }
    return true
  }
}
