import { FLAG_CLAIM } from "src/constants"
import { filterFlagsByRoomAndType } from "src/flags"

export const roleClaimer = creep => {
  let { targetRoom } = creep.memory
  if (targetRoom) {
    if (targetRoom != creep.pos.roomName) {
      creep.moveTo(new RoomPosition(25, 25, targetRoom))
    } else {
      if (creep.room.controller.my) {
        return true
      }

      let target = creep.room.controller
      let claimResult = creep.claimController(target)
      if (claimResult == ERR_NOT_IN_RANGE) {
        creep.moveTo(target, {
          visualizePathStyle: { stroke: "#ffffff" }
        })
        return true
      }
      if (_.includes([OK, ERR_BUSY], claimResult)) {
        return true
      }

      console.log("claim failed", claimResult)
    }
    return true
  }

  let claimFlags = filterFlagsByRoomAndType(
    _.values(Game.flags),
    Game.rooms[creep.memory.roomName] || creep.room,
    FLAG_CLAIM
  )

  _.each(claimFlags, reserveFlag => {
    let { roomName } = reserveFlag.pos
    let memory = Memory.rooms[reserveFlag.pos.roomName]
    if (!memory) {
      return
    }
    if (memory.owner === null) {
      creep.memory.targetRoom = roomName
    }
  })
  return true
}
