import { registerFN } from "src/screeps-profiler"

import { LAB_REACTION, LAB_REAGENT } from "src/constants"

declare global {
  interface LabMemory {
    type: "lab_reagent" | "lab_reaction" | ""
    mineralType: ResourceConstant | ""
    reactionType: ResourceConstant[] | undefined[]
    boostingTarget: string
    updatedAt: number
    reagentLabIds: string[]
    debug: boolean
    targetReaction: ResourceConstant | ""
  }

  interface StructureLab {
    memory: LabMemory
  }
}

const _processReactionLab = (lab: StructureLab) => {
  if (lab.cooldown > 0) {
    return true
  }

  try {
    if (!lab.memory.reactionType) {
      throw new Error("No possible reactions")
    }

    let result = lab.runReaction(
      lab.room.reagentLabs[0],
      lab.room.reagentLabs[1]
    )

    if (_.includes([OK, ERR_TIRED], result)) {
      return true
    }
    if (result == ERR_NOT_IN_RANGE) {
      _.each(lab.room.labs, l => {
        l.memory.type = ""
      })
      return true
    }
    throw new Error(`runReaction failed ${result}`)
  } catch (e) {
    if (lab.memory.debug) {
      console.log(lab, e)
    }
    lab.memory.type = ""
    lab.memory.updatedAt = Game.time
  }
}

export const processReactionLab: (lab: StructureLab) => void = registerFN(
  _processReactionLab,
  "lab.processReactionLab"
)

const _processReagentLab = (lab: StructureLab) => {
  let { room, memory } = lab
  if (memory.mineralType && memory.targetReaction == room.memory.reaction) {
    return
  }

  if (lab.room.reactionType) {
    const reagentLabs = _.filter(
      lab.room.labs,
      (reagentLab: StructureLab) => reagentLab.memory.type == LAB_REAGENT
    )

    const mineralType = _(lab.room.reactionType)
      .filter(mineral => {
        return !_.some(
          reagentLabs,
          reagentLab => reagentLab.memory.mineralType == mineral
        )
      })
      .first()

    memory.mineralType = mineralType
    memory.targetReaction = room.memory.reaction
  }
}

export const processReagentLab: (StructureLab) => void = registerFN(
  _processReagentLab,
  "lab.processReagentLab"
)

const _defineLabType = (lab: StructureLab) => {
  const { room, pos } = lab
  const reagentLabs = _.filter(
    lab.room.labs,
    (lab: StructureLab) => lab.memory.type == LAB_REAGENT
  )
  lab.memory.mineralType = ""
  if (reagentLabs.length >= 2) {
    lab.memory.type = LAB_REACTION
    lab.memory.reactionType = room.reactionType
    lab.memory.mineralType = room.memory.reaction
    return
  }
  if (pos.findInRange(room.labs, 2).length == room.labs.length) {
    lab.memory.type = LAB_REAGENT
  }
}

export const defineLabType: (StructureLab) => void = registerFN(
  _defineLabType,
  "lab.defineLabType"
)

const _processLab = (lab: StructureLab) => {
  if (lab.memory.boostingTarget) {
    const boostingTarget: Creep = Game.getObjectById(lab.memory.boostingTarget)
    if (!boostingTarget || !lab.pos.isNearTo(boostingTarget)) {
      lab.memory.boostingTarget = undefined
    } else {
      lab.boostCreep(boostingTarget)
    }
  }

  switch (lab.memory.type) {
    case LAB_REACTION:
      return processReactionLab(lab)
    case LAB_REAGENT:
      return processReagentLab(lab)
    default:
      return defineLabType(lab)
  }
}

export const processLab: (StructureLab) => void = registerFN(
  _processLab,
  "lab.processLab"
)
