export const ROLE_BUILDER = "builder"
export const ROLE_HARVESTER = "harvester"
export const ROLE_MINERAL_HARVESTER = "mineralHarvester"
export const ROLE_UPGRADER = "upgrader"
export const ROLE_COURIER = "courier"
export const ROLE_SCOUT = "scout"
export const ROLE_DEMOLISHER = "demolisher"
export const ROLE_EXTERNAL_HARVESTER = "externalHarvester"
export const ROLE_CLAIMER = "claimer"
export const ROLE_ATTACKER = "attacker"

export const FLAG_HARVEST = "harvest"
export const FLAG_UPGRADE = "upgrade"
export const FLAG_RESERVE = "reserve"
export const FLAG_CLAIM = "claim"
export const FLAG_MINERAL = "mineral"
export const FLAG_GATHER = "gather"

export const LINK_STORAGE = "link_storage"
export const LINK_HARVEST = "link_harvest"
export const LINK_CONTROLLER = "link_controller"

export const LAB_REAGENT = "lab_reagent"
export const LAB_REACTION = "lab_reaction"

export const PART_COSTS = {
  [MOVE]: 50,
  [WORK]: 100,
  [CARRY]: 50,
  [ATTACK]: 80,
  [RANGED_ATTACK]: 150,
  [HEAL]: 250,
  [CLAIM]: 600,
  [TOUGH]: 10
}

const mapReactions = reagent => {
  return _.map(REACTIONS[reagent], (v, k) => [v, [k, reagent]])
}

const allReactions: any = _(REACTIONS)
  .keys()
  .map(mapReactions)
  .flatten()
  .zipObject()
  .value()

export const REACTION_TYPES: Map<
  ResourceConstant,
  [ResourceConstant, ResourceConstant]
> = allReactions

export const REACTION_TARGETS = {
  GH2O: 100000,
  XUHO2: 30000,
  XUH2O: 30000,
  XKHO2: 30000,
  XKH2O: 30000,
  XLHO2: 30000,
  XLH2O: 30000,
  XZHO2: 30000,
  XZH2O: 30000,
  XGHO2: 30000,
  XGH2O: 100000000
}
