import { FLAG_MINERAL } from "src/constants";
import { findFlagsByType } from "src/flags";

export const roleMineralHarvester = creep => {
  if (creep.memory.targetFlag) {
    const flag = Game.flags[creep.memory.targetFlag];
    if (!flag) {
      creep.memory.targetFlag = undefined;
      return false;
    }
    const mineral = Game.getObjectById(flag.memory.mineralId);
    const result = creep.harvest(mineral);
    if (creep.pos != flag.pos) {
      creep.moveTo(flag, {
        visualizePathStyle: { stroke: "#ffaa00" }
      });
      return true;
    }
    if (result == OK || result == ERR_INVALID_TARGET || result == ERR_BUSY) {
      return true;
    }
    console.log(result, creep);
    creep.say("Lazy ⛏");
  } else {
    let flags = findFlagsByType(creep.room, FLAG_MINERAL);
    creep.memory.targetFlag = _.get(_.first(flags), "name");
  }

  return false;
};
