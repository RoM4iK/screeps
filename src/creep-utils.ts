export const getResources = (creep: Creep, pickupMinerals = false) => {
  if (creep.memory.pickupTarget) {
    let pickupTarget: Resource = Game.getObjectById(creep.memory.pickupTarget)
    if (pickupTarget) {
      let pickupResult = creep.pickup(pickupTarget)
      if (pickupResult == ERR_NOT_IN_RANGE) {
        creep.moveTo(pickupTarget, {
          visualizePathStyle: { stroke: "#ffaa00" }
        })
        return true
      }
      if (pickupResult == OK || pickupResult == ERR_BUSY) {
        return true
      }

      if (pickupResult == ERR_INVALID_TARGET) {
        creep.memory.pickupTarget = undefined
      }
    } else {
      creep.memory.pickupTarget = undefined
    }
  }

  if (creep.memory.withdrawTarget) {
    let withdrawTarget: Structure = Game.getObjectById(
      creep.memory.withdrawTarget
    )
    if (withdrawTarget && creep.memory.withdrawType) {
      let withdrawResult = creep.withdraw(
        withdrawTarget,
        creep.memory.withdrawType
      )
      if (withdrawResult == ERR_NOT_IN_RANGE) {
        creep.moveTo(withdrawTarget, {
          visualizePathStyle: { stroke: "#ffaa00" }
        })
        return true
      }
      if (withdrawResult == OK || withdrawResult == ERR_BUSY) {
        return true
      }

      if (withdrawResult == ERR_NOT_ENOUGH_RESOURCES) {
        creep.memory.withdrawTarget = undefined
        creep.memory.withdrawType = undefined
      }
    } else {
      creep.memory.withdrawTarget = undefined
      creep.memory.withdrawType = undefined
    }
  }

  let { storage, terminal } = creep.room

  if (storage) {
    if (pickupMinerals) {
      setPickupTarget(creep, pickupMinerals)
      if (creep.memory.pickupTarget) {
        return false
      }

      setWithdrawFromTombstone(creep)

      if (creep.memory.withdrawTarget) {
        return false
      }

      setWithdrawFromStorageLink(creep)

      if (creep.memory.withdrawTarget) {
        return false
      }

      setWithdrawFromMineralContainer(creep)

      if (creep.memory.withdrawTarget) {
        return false
      }
      setWithdrawFromHarvestContainers(creep)

      if (creep.memory.withdrawTarget) {
        return false
      }

      let fullLab = _.first(
        _.filter(
          creep.room.labs,
          lab =>
            lab.mineralAmount >= 2500 ||
            (lab.mineralType && lab.mineralType != lab.memory.mineralType)
        )
      )

      if (fullLab) {
        creep.memory.withdrawTarget = fullLab.id
        creep.memory.withdrawType = fullLab.mineralType
        return false
      }

      _.some(creep.room.labs, lab => {
        if (lab.mineralAmount < 1000) {
          let { mineralType } = lab.memory
          if (storage && storage.store[mineralType] > 0) {
            creep.memory.withdrawTarget = storage.id
            creep.memory.withdrawType = mineralType
            return true
          }

          if (terminal && terminal.store[mineralType] > 0) {
            creep.memory.withdrawTarget = terminal.id
            creep.memory.withdrawType = mineralType
            return true
          }
        }
      })

      if (creep.memory.withdrawTarget) {
        return false
      }

      if (terminal) {
        if (_.sum(terminal.store) < 295000) {
          let trasferableResources = _.filter(
            _.keys(storage.store),
            type =>
              type !== RESOURCE_ENERGY &&
              storage.store[type] > 0 &&
              (terminal.store[type] || 0) < 30000
          )

          if (trasferableResources.length) {
            creep.memory.withdrawTarget = storage.id
            creep.memory.withdrawType = trasferableResources[0]
            return false
          }
        }
      }
    }
  } else {
    setWithdrawFromStorageLink(creep)
    if (!creep.memory.withdrawTarget) {
      setWithdrawFromHarvestContainers(creep)
    }

    if (!creep.memory.withdrawTarget) {
      if (creep.room.storage) {
        creep.memory.withdrawTarget = storage.id
      }
    }

    if (!creep.memory.withdrawTarget) {
      setPickupTarget(creep, pickupMinerals)
    }
  }

  let target = _([terminal, storage])
    .compact()
    .filter(s => s.store[RESOURCE_ENERGY] > 0)
    .sortBy(s => s.store[RESOURCE_ENERGY])
    .last()

  if (target) {
    if (creep.memory.debug) {
      console.log(
        creep,
        creep.room,
        `Withdraw from ${target.structureType}`,
        target
      )
    }
    creep.memory.withdrawTarget = target.id
    creep.memory.withdrawType = RESOURCE_ENERGY
  }
}

const setPickupTarget = (creep: Creep, pickupMinerals = false) => {
  const resource = creep.pos.findClosestByPath(FIND_DROPPED_RESOURCES, {
    filter: resource => {
      if (resource.resourceType == RESOURCE_ENERGY && resource.amount < 100) {
        return false
      }
      if (resource.resourceType != RESOURCE_ENERGY && !pickupMinerals) {
        return false
      }
      let concurents = _.filter(
        Game.creeps,
        c => c.memory.pickupTarget == resource.id
      )
      let claimedCapacity = _.reduce(
        concurents,
        (total: number, c: Creep) => total + c.carryCapacity,
        0
      )
      return resource.amount > claimedCapacity
    }
  })
  if (resource) {
    creep.memory.pickupTarget = resource.id
  }
}

const setWithdrawFromTombstone = (creep: Creep) => {
  const tombstone = creep.pos.findClosestByPath(FIND_TOMBSTONES, {
    filter: tombstone => {
      const resourcesAmount = _.sum(_.values(tombstone.store))
      const energyAmount = tombstone.store[RESOURCE_ENERGY]
      const mineralsAmount = resourcesAmount - energyAmount
      if (mineralsAmount === 0 && energyAmount < 100) {
        return false
      }
      let concurents = _.filter(
        Game.creeps,
        c => c.memory.withdrawTarget == tombstone.id
      )
      let claimedCapacity = _.reduce(
        concurents,
        (total: number, c: Creep) => total + c.carryCapacity,
        0
      )

      return resourcesAmount > claimedCapacity
    }
  })
  if (tombstone) {
    if (creep.memory.debug) {
      console.log(creep, creep.room, "Withdraw from tombstone", tombstone)
    }
    creep.memory.withdrawTarget = tombstone.id
    creep.memory.withdrawType =
      _.first(_.without(_.keys(tombstone.store), RESOURCE_ENERGY)) ||
      RESOURCE_ENERGY
  }
}

const setWithdrawFromMineralContainer = (creep: Creep) => {
  const container: any = creep.pos.findClosestByPath(FIND_STRUCTURES, {
    filter: container => {
      if (container.structureType !== STRUCTURE_CONTAINER) {
        return false
      }

      const resourcesAmount = _.sum(_.values(container.store))
      const energyAmount = container.store[RESOURCE_ENERGY]
      const mineralsAmount = resourcesAmount - energyAmount
      if (mineralsAmount < creep.carryCapacity) {
        return false
      }
      let concurents = _.filter(
        Game.creeps,
        c => c.memory.withdrawTarget == container.id
      )
      let claimedCapacity = _.reduce(
        concurents,
        (total: number, c: Creep) => total + c.carryCapacity,
        0
      )

      return resourcesAmount > claimedCapacity + creep.carryCapacity * 0.75
    }
  })
  if (container) {
    if (creep.memory.debug) {
      console.log(
        creep,
        creep.room,
        "Withdraw from mineral container",
        container
      )
    }
    creep.memory.withdrawTarget = container.id
    creep.memory.withdrawType = _.first(
      _.without(_.keys(container["store"]), RESOURCE_ENERGY)
    )
  }
}

const setWithdrawFromHarvestContainers = (creep: Creep) => {
  const container = creep.pos.findClosestByPath(creep.room.harvestContainers, {
    filter: (container: StructureContainer) => {
      const energyAmount = container.store[RESOURCE_ENERGY]
      let concurents = _.filter(
        Game.creeps,
        c => c.memory.withdrawTarget == container.id
      )
      let claimedCapacity = _.reduce(
        concurents,
        (total: number, c: Creep) => total + c.carryCapacity,
        0
      )

      return energyAmount > claimedCapacity + creep.carryCapacity * 0.75
    }
  })

  if (container) {
    if (creep.memory.debug) {
      console.log(
        creep,
        creep.room,
        "Withdraw from harvest container",
        container
      )
    }
    creep.memory.withdrawTarget = container.id
    creep.memory.withdrawType = RESOURCE_ENERGY
  }
}

const setWithdrawFromStorageLink = (creep: Creep) => {
  let { storageLinks } = creep.room
  if (!storageLinks) {
    return false
  }
  return _.some(storageLinks, storageLink => {
    let concurents = _.filter(
      Game.creeps,
      c => c.memory.withdrawTarget == storageLink.id
    )
    let claimedCapacity = _.reduce(
      concurents,
      (total: number, c: Creep) => total + c.carryCapacity,
      0
    )
    if (storageLink.energy > claimedCapacity + creep.carryCapacity * 0.75) {
      if (creep.memory.debug) {
        console.log(
          creep,
          creep.room,
          "Withdraw from storage link",
          storageLink
        )
      }
      creep.memory.withdrawTarget = storageLink.id
      creep.memory.withdrawType = RESOURCE_ENERGY
      return true
    }
  })
}

export const processBoosting = (creep: Creep, boostingHash: Object) => {
  if (!creep.memory.boostingFinished) {
    const boostingMinerals = _(creep.body)
      .filter(part => part.boost == undefined)
      .map(part => boostingHash[part.type])
      .unique()
      .compact()
      .value()

    const boostingLabs = _.filter(
      creep.room.labs,
      lab =>
        _.includes(boostingMinerals, lab.mineralType) && lab.mineralAmount > 30
    )
    if (!_.isEmpty(boostingLabs)) {
      const boostingLab = _.first(boostingLabs)
      creep.moveTo(boostingLab)
      boostingLab.memory.boostingTarget = creep.id
      return true
    }

    creep.memory.boostingFinished = true
  }
}

export const getEnergyDeliveryTarget = creep => {
  let target = creep.pos.findClosestByPath(FIND_STRUCTURES, {
    filter: structure => {
      return (
        _.includes(
          [STRUCTURE_EXTENSION, STRUCTURE_SPAWN],
          structure.structureType
        ) && structure.energy < structure.energyCapacity
      )
    }
  })

  if (target) {
    return target
  }

  target = creep.pos.findClosestByPath(FIND_STRUCTURES, {
    filter: structure => {
      return (
        _.includes([STRUCTURE_TOWER, STRUCTURE_LAB], structure.structureType) &&
        structure.energy < structure.energyCapacity * 0.5
      )
    }
  })

  if (target) {
    return target
  }

  target = creep.pos.findClosestByPath(FIND_STRUCTURES, {
    filter: structure => {
      return structure.structureType == STRUCTURE_LAB && structure.energy < 1500
    }
  })

  if (target) {
    return target
  }

  target = _.find(
    creep.room.upgradeContainers,
    (container: StructureContainer) =>
      container.store[RESOURCE_ENERGY] < container.storeCapacity * 0.75
  )

  if (target) {
    return target
  }

  const { terminal, storage, nuker, powerSpawn } = Game.rooms[
    creep.memory.roomName
  ]

  if (terminal && terminal.store[RESOURCE_ENERGY] < 20000) {
    target = terminal
  } else if (powerSpawn && powerSpawn.energy < powerSpawn.energyCapacity) {
    target = powerSpawn
  } else if (nuker && nuker.energy < nuker.energyCapacity) {
    target = nuker
  }

  if (!target) {
    target = storage
  }

  return target
}
