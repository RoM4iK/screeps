import { createCachedGetter, createGroupedGetter } from "src/utils"
import {
  FLAG_HARVEST,
  FLAG_UPGRADE,
  FLAG_GATHER,
  LINK_STORAGE,
  LINK_HARVEST,
  LINK_CONTROLLER,
  LAB_REAGENT,
  LAB_REACTION,
  REACTION_TYPES,
  FLAG_CLAIM,
  FLAG_MINERAL,
  FLAG_RESERVE
} from "src/constants"

declare global {
  interface Room {
    creeps: Creep[]
    structures: Structure[]
    spawns: StructureSpawn[]
    towers: StructureTower[]
    links: StructureLink[]
    harvestLinks: StructureLink[]
    controllerLink: StructureLink
    storageLinks: StructureLink[]
    labs: StructureLab[]
    reactionLabs: StructureLab[]
    reagentLabs: StructureLab[]
    harvestFlags: Flag[]
    upgradeFlags: Flag[]
    mineralFlags: Flag[]
    claimFlags: Flag[]
    reserveFlags: Flag[]
    gatherFlag: Flag
    harvestContainers: StructureContainer[]
    groupedStructures: Map<StructureConstant, Structure[]>
    nuker: StructureNuker
    powerSpawn: StructurePowerSpawn
    observer: StructureObserver
    observeTarget: string
    roomStore: StoreDefinition
    haveStorage: boolean
    haveTerminal: boolean
    reaction: ResourceConstant | ""
    reactionType: ResourceConstant[]
    constructionSites: ConstructionSite[]
  }

  interface LinkMemory {
    type: "link_storage" | "link_harvest" | "link_controller"
  }

  interface StructureLink {
    memory: LinkMemory
  }
}

export const createDefinitions = () => {
  createCachedGetter(Room.prototype, "structures", function() {
    return this.find(FIND_STRUCTURES)
  })

  createCachedGetter(Room.prototype, "constructionSites", function() {
    return this.find(FIND_MY_CONSTRUCTION_SITES)
  })

  createCachedGetter(Room.prototype, "creeps", function() {
    return _.filter(Game.creeps, c => c.memory.roomName === this.name)
  })

  createCachedGetter(Room.prototype, "groupedStructures", function() {
    return _.groupBy(this.structures, "structureType")
  })

  createGroupedGetter(
    Room.prototype,
    "spawns",
    "groupedStructures",
    STRUCTURE_SPAWN
  )
  createGroupedGetter(
    Room.prototype,
    "towers",
    "groupedStructures",
    STRUCTURE_TOWER
  )
  createGroupedGetter(
    Room.prototype,
    "links",
    "groupedStructures",
    STRUCTURE_LINK
  )
  createGroupedGetter(
    Room.prototype,
    "labs",
    "groupedStructures",
    STRUCTURE_LAB
  )

  createCachedGetter(Room.prototype, "reagentLabs", function() {
    return _.filter(
      this.labs,
      (lab: StructureLab) => lab.memory.type == LAB_REAGENT
    )
  })

  createCachedGetter(Room.prototype, "reactionLabs", function() {
    return _.filter(
      this.labs,
      (lab: StructureLab) => lab.memory.type == LAB_REACTION
    )
  })

  createCachedGetter(Room.prototype, "nuker", function() {
    return _.first(this.groupedStructures[STRUCTURE_NUKER])
  })

  createCachedGetter(Room.prototype, "powerSpawn", function() {
    return _.first(this.groupedStructures[STRUCTURE_POWER_SPAWN])
  })

  createCachedGetter(Room.prototype, "observer", function() {
    return _.first(this.groupedStructures[STRUCTURE_OBSERVER])
  })

  createCachedGetter(Room.prototype, "storageLinks", function() {
    return _.filter(
      this.links,
      (link: StructureLink) => _.get(link, "memory.type") == LINK_STORAGE
    )
  })

  createCachedGetter(Room.prototype, "controllerLink", function() {
    return _(this.links)
      .filter((link: StructureLink) => link.memory.type == LINK_CONTROLLER)
      .first()
  })

  createCachedGetter(Room.prototype, "harvestLinks", function() {
    return _.filter(
      this.links,
      (link: StructureLink) => link.memory.type == LINK_HARVEST
    )
  })

  createCachedGetter(Room.prototype, "harvestFlags", function() {
    return this.find(FIND_FLAGS, {
      filter: (flag: Flag) => flag.memory.type == FLAG_HARVEST
    })
  })

  createCachedGetter(Room.prototype, "upgradeFlags", function() {
    return this.find(FIND_FLAGS, {
      filter: (flag: Flag) => flag.memory.type == FLAG_UPGRADE
    })
  })

  createCachedGetter(Room.prototype, "mineralFlags", function() {
    return this.find(FIND_FLAGS, {
      filter: (flag: Flag) => flag.memory.type == FLAG_MINERAL
    })
  })

  createCachedGetter(Room.prototype, "flags", function() {
    return _.filter(Game.flags, flag => flag.memory.roomName == this.name)
  })

  createCachedGetter(Room.prototype, "groupedFlags", function() {
    return _.groupBy(this.flags, "memory.type")
  })

  createCachedGetter(Room.prototype, "gatherFlag", function() {
    return _.first(this.groupedFlags[FLAG_GATHER])
  })

  createGroupedGetter(Room.prototype, "claimFlags", "groupedFlags", FLAG_CLAIM)
  createGroupedGetter(
    Room.prototype,
    "reserveFlags",
    "groupedFlags",
    FLAG_RESERVE
  )

  createCachedGetter(Room.prototype, "observeTarget", function() {
    return _(this.reserveFlags)
      .concat(this.claimFlags)
      .map(flag => flag.pos.roomName)
      .sortBy(roomName => {
        const memory = Memory.rooms[roomName]
        if (!memory) {
          Memory.rooms[roomName] = {}
        }
        return (memory && memory.updatedAt) || 0
      })
      .first()
  })

  createCachedGetter(Room.prototype, "harvestContainers", function() {
    return _(this.harvestFlags)
      .map((flag: Flag) => this.lookForAt(LOOK_STRUCTURES, flag.pos))
      .flatten()
      .filter(
        (structure: Structure) => structure.structureType == STRUCTURE_CONTAINER
      )
      .value()
  })

  createCachedGetter(Room.prototype, "upgradeContainers", function() {
    return _(this.upgradeFlags)
      .map((flag: Flag) => Game.getObjectById(flag.memory.containerId))
      .compact()
      .value()
  })

  createCachedGetter(Room.prototype, "roomStore", function() {
    let room: Room = this
    const { storage, terminal } = room

    if (!storage && !terminal) {
      return {}
    }

    if (storage && !terminal) {
      return storage.store
    }

    if (!storage && terminal) {
      return terminal.store
    }

    return _(_.keys(storage.store))
      .concat(_.keys(terminal.store))
      .uniq()
      .map(k => [k, _.sum([storage.store[k], terminal.store[k]])])
      .zipObject()
      .value()
  })

  createCachedGetter(Room.prototype, "haveStorage", function() {
    return !!_.get(this, "storage.my")
  })

  createCachedGetter(Room.prototype, "haveTerminal", function() {
    return !!_.get(this, "terminal.my")
  })

  createCachedGetter(Room.prototype, "reactionType", function() {
    if (!this.reaction) {
      return []
    }
    return REACTION_TYPES[this.memory.reaction.toUpperCase()]
  })

  createCachedGetter(Room.prototype, "reaction", function() {
    return this.memory.reaction
  })

  if (Memory.links === undefined) {
    Memory.links = {}
  }

  createCachedGetter(StructureLink.prototype, "memory", function() {
    if (Memory.links[this.id] === undefined) {
      Memory.links[this.id] = {}
    }
    return Memory.links[this.id]
  })

  if (Memory.labs === undefined) {
    Memory.labs = {}
  }

  createCachedGetter(StructureLab.prototype, "memory", function() {
    if (Memory.labs[this.id] === undefined) {
      Memory.labs[this.id] = {}
    }
    return Memory.labs[this.id]
  })

  if (!Memory.market) {
    Memory.market = {}
  }

  if (!Memory.impassableRooms) {
    Memory.impassableRooms = []
  }
}
