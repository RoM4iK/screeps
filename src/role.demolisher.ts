import { FLAG_CLAIM, ROLE_DEMOLISHER } from "src/constants"
import { filterFlagsByRoomAndType } from "src/flags"
import { processBoosting } from "src/creep-utils"

const DEMOLISHER_BOOSTING = {
  tough: "XGHO2",
  heal: "XLHO2",
  work: "XZH2O"
}

export const roleDemolisher = (creep: Creep) => {
  creep.disableNotifications()
  let isBoosting = processBoosting(creep, DEMOLISHER_BOOSTING)
  if (isBoosting) {
    return true
  }

  let damagedCreep = _.first(
    creep.room.find(FIND_MY_CREEPS, {
      filter: c => c.hits < c.hitsMax - 600
    })
  )
  if (damagedCreep) {
    if (!creep.pos.isNearTo(damagedCreep)) {
      creep.moveTo(damagedCreep)
      return true
    }
    creep.heal(damagedCreep)
    creep.say("Healing")
    return true
  }

  const demolishersSquadSize =
    _.get(
      Game.rooms[creep.memory.roomName],
      "memory.spawnCreepsCount.demolishersCount"
    ) || 2

  let demolisherCreeps = _.filter(
    Game.creeps,
    c =>
      c.memory.roomName === creep.memory.roomName &&
      c.memory.role == ROLE_DEMOLISHER &&
      c.memory.readyForSiege == true
  )

  if (demolisherCreeps.length < demolishersSquadSize) {
    if (creep.memory.readyForSiege) {
      return true
    }
    let { gatherFlag } = creep.room
    if (gatherFlag) {
      if (creep.pos.isNearTo(gatherFlag)) {
        creep.memory.readyForSiege = true
        return true
      } else {
        creep.moveTo(gatherFlag, { visualizePathStyle: { stroke: "#ffffff" } })
        return true
      }
    } else {
      creep.memory.readyForSiege = true
      return true
    }
  }

  let { targetRoom } = creep.memory

  if (targetRoom) {
    if (targetRoom != creep.pos.roomName) {
      const targetRoomPosition = new RoomPosition(25, 25, targetRoom)
      creep.moveTo(targetRoomPosition, {
        visualizePathStyle: { stroke: "#ffffff" }
      })
    } else {
      if (creep.room.memory.dismantleTargetId) {
        const dismantleTarget: Structure = Game.getObjectById(
          creep.room.memory.dismantleTargetId
        )
        if (!dismantleTarget) {
          creep.room.memory.dismantleTargetId = null
          return false
        }
        let dismantleResult = creep.dismantle(dismantleTarget)

        if (dismantleResult == ERR_NOT_IN_RANGE) {
          let moveResult = creep.moveTo(dismantleTarget, {
            visualizePathStyle: { stroke: "#ffffff" }
          })
          if (_.includes([OK, ERR_BUSY, ERR_TIRED], moveResult)) {
            return true
          }
          if (moveResult == ERR_NO_PATH) {
            let path = creep.pos.findPathTo(dismantleTarget, {
              ignoreDestructibleStructures: true
            })
            let blocker: Structure = _.first(
              creep.room.lookForAt(LOOK_STRUCTURES, path[0].x, path[0].y)
            )
            if (blocker) {
              creep.room.memory.dismantleTargetId = blocker.id
            }
            return false
          }
          return true
        }
        if (_.includes([OK, ERR_BUSY], dismantleResult)) {
          creep.say("hello")
          return true
        }

        console.log("dismantle failed", dismantleResult)
        creep.room.memory.dismantleTargetId = null
        return false
      }

      let target = creep.pos.findClosestByRange(FIND_HOSTILE_STRUCTURES, {
        filter: s => s.structureType == STRUCTURE_TOWER && s.hits > 0
      })

      if (!target) {
        target = creep.pos.findClosestByRange(FIND_HOSTILE_STRUCTURES, {
          filter: s => s.structureType == STRUCTURE_SPAWN && s.hits > 0
        })
      }

      if (target) {
        creep.room.memory.dismantleTargetId = target.id
        return false
      }

      creep.memory.targetRoom = null
      return true
    }
    return true
  }

  _.each(creep.room.claimFlags.concat(creep.room.reserveFlags), flag => {
    let { roomName } = flag.pos
    let memory = Memory.rooms[flag.pos.roomName]
    if (!memory) {
      return
    }
    if (memory.towersCount > 0 || memory.spawnsCount > 0) {
      creep.memory.targetRoom = roomName
    }
  })
  return true
}
