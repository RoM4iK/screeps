import { registerFN } from "src/screeps-profiler"

import { LINK_STORAGE, LINK_HARVEST, LINK_CONTROLLER } from "src/constants"

if (Memory.links === undefined) {
  Memory.links = {}
}

const _processHarvestLink = (link: StructureLink) => {
  if (link.cooldown > 0 || link.energy < 100) {
    return true
  }
  let targetLinks = _.filter(link.room.storageLinks, _.identity)
  if (link.room.controllerLink) {
    targetLinks.unshift(link.room.controllerLink)
  }

  let targetLink = _(targetLinks)
    .filter(l => l.energy < l.energyCapacity)
    .sortBy("energy")
    .first()

  if (targetLink) {
    let transferResult = link.transferEnergy(targetLink)
    if (_.includes([OK, ERR_TIRED], transferResult)) {
      return true
    }
    console.log(link, link.room, transferResult)
  }
}

const processHarvestLink: (link: StructureLink) => void = registerFN(
  _processHarvestLink,
  "link.processHarvestLink"
)

const _defineLinkType = (link: StructureLink) => {
  if (link.pos.findInRange(link.room.upgradeFlags, 2).length > 0) {
    link.memory.type = LINK_CONTROLLER
    return
  }

  if (link.pos.inRangeTo(link.room.storage, 2)) {
    link.memory.type = LINK_STORAGE
    return
  }

  if (link.pos.findInRange(link.room.harvestFlags, 2).length > 0) {
    link.memory.type = LINK_HARVEST
    return
  }

  console.log(link, link.room, "Cannot define link type")
}

const defineLinkType: (StructureLink) => void = registerFN(
  _defineLinkType,
  "link.defineLinkType"
)

const _processLink = (link: StructureLink) => {
  switch (link.memory.type) {
    case LINK_HARVEST:
      return processHarvestLink(link)
    case LINK_CONTROLLER:
      return
    case LINK_STORAGE:
      return
    default:
      return defineLinkType(link)
  }
}

export const processLink: (StructureLink) => void = registerFN(
  _processLink,
  "link.processLink"
)
