import { FLAG_CLAIM, FLAG_RESERVE } from "src/constants"
import { filterFlagsByRoomAndType } from "src/flags"

export const roleAttacker = creep => {
  creep.disableNotifications()
  let { targetRoom } = creep.memory

  if (targetRoom) {
    if (targetRoom != creep.pos.roomName) {
      const targetRoomPosition = new RoomPosition(25, 25, targetRoom)
      creep.moveTo(targetRoomPosition, {
        visualizePathStyle: { stroke: "#ffffff" }
      })
    } else {
      let target = creep.room.controller
      let claimResult = creep.attackController(target)
      if (claimResult == ERR_NOT_IN_RANGE) {
        creep.moveTo(target, {
          visualizePathStyle: { stroke: "#ffffff" }
        })
        return true
      }
      if (_.includes([OK, ERR_BUSY, ERR_TIRED], claimResult)) {
        return true
      }

      console.log("claim failed", claimResult)
      return true
    }
    return true
  }

  let reserveFlags = filterFlagsByRoomAndType(
    _.values(Game.flags),
    Game.rooms[creep.memory.roomName] || creep.room,
    FLAG_RESERVE
  )

  let claimFlags = filterFlagsByRoomAndType(
    _.values(Game.flags),
    Game.rooms[creep.memory.roomName] || creep.room,
    FLAG_CLAIM
  )

  // TODO: optimize attack target

  _.each(reserveFlags.concat(claimFlags), (flag: Flag) => {
    let { roomName } = flag.pos
    let memory = Memory.rooms[flag.pos.roomName]
    if (!memory) {
      return
    }
    if (memory.towersCount == 0 && memory.owner !== null) {
      creep.memory.targetRoom = roomName
    }
  })

  return true
}
