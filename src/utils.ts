export const createCachedGetter = (
  object: object,
  key: string,
  getter: Function
): void => {
  if (!object.hasOwnProperty(key)) {
    Object.defineProperty(object, key, {
      get: function() {
        if (this[`__${key}`]) {
          return this[`__${key}`]
        }
        this[`__${key}`] = getter.call(this)
        return this[`__${key}`]
      }
    })
  }
}

export const createGroupedGetter = (
  obj: object,
  key: string,
  groupKey: string,
  nestedKey: string
) => {
  createCachedGetter(obj, key, function() {
    return _.get(this[groupKey], nestedKey) || []
  })
}
