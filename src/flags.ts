import { FLAG_HARVEST, FLAG_UPGRADE, FLAG_MINERAL } from "src/constants"

const createFlag = (room, { x, y }, memory) => {
  const flagName = room.createFlag(x, y)
  const flag = Game.flags[flagName]
  flag.memory = {
    roomName: room.name,
    ...memory
  }
}

export const createHarvestFlags = (room: Room) => {
  const { harvestFlags } = room
  let sources = room.find(FIND_SOURCES)
  if (harvestFlags.length == sources.length) {
    return true
  }

  _.each(sources, source => {
    const haveFlag = _.some(
      harvestFlags,
      flag => flag.memory.sourceId == source.id
    )
    if (!haveFlag) {
      let containers = source.pos.findInRange(FIND_STRUCTURES, 1, {
        filter: s => s.structureType == STRUCTURE_CONTAINER
      })

      let constructionSites = source.pos.findInRange(
        FIND_MY_CONSTRUCTION_SITES,
        1,
        {
          filter: cs => cs.structureType == STRUCTURE_CONTAINER
        }
      )

      _.each(containers, container => {
        createFlag(room, container.pos, {
          type: FLAG_HARVEST,
          sourceId: source.id
        })
      })

      _.each(constructionSites, cs => {
        createFlag(room, cs.pos, {
          type: FLAG_HARVEST,
          sourceId: source.id
        })
      })
      return true
    }
  })
}

export const createMineralFlags = room => {
  const mineralFlags = findFlagsByType(room, FLAG_MINERAL)
  if (mineralFlags.length > 0) {
    return true
  }
  let extractor = room.find(FIND_STRUCTURES, {
    filter: f => f.structureType === STRUCTURE_EXTRACTOR
  })
  let mineral: Mineral = _.first(room.find(FIND_MINERALS))
  if (!extractor.length) {
    return true
  }

  let containers = mineral.pos.findInRange(FIND_STRUCTURES, 1, {
    filter: s => s.structureType == STRUCTURE_CONTAINER
  })

  _.each(containers, container => {
    const haveFlag = _.some(
      mineralFlags,
      flag => flag.memory.containerId == container.id
    )
    if (!haveFlag) {
      createFlag(room, container.pos, {
        type: FLAG_MINERAL,
        mineralId: mineral.id
      })
      return true
    }
  })

  return true
}

export const createUpgradeFlags = room => {
  const upgradeFlags: Flag[] = findFlagsByType(room, FLAG_UPGRADE)
  const controller = room.controller
  if (!controller) {
    return true
  }
  _.each(upgradeFlags, flag => {
    if (!Game.getObjectById(flag.memory.containerId)) {
      flag.remove()
    }
  })
  let containers = controller.pos.findInRange(FIND_STRUCTURES, 1, {
    filter: s => s.structureType == STRUCTURE_CONTAINER
  })
  if (upgradeFlags.length == containers.length) {
    return true
  }

  _.each(containers, container => {
    const haveFlag = _.some(
      upgradeFlags,
      flag => flag.memory.containerId == container.id
    )
    // return true
    if (!haveFlag) {
      createFlag(room, container.pos, {
        type: FLAG_UPGRADE,
        containerId: container.id
      })
      return true
    }
  })
}

export const findFlagsByType = (room, type): Flag[] => {
  if (!room) {
    return []
  }
  if (!type) {
    console.log("No type specified")
    return []
  }
  return room.find(FIND_FLAGS, {
    filter: flag => flag.memory.type == type
  })
}

export const filterFlagsByType = (flags, type): Flag[] =>
  _.filter(flags, flag => flag.memory.type === type)

export const filterFlagsByRoom = (flags: Flag[], room): Flag[] =>
  _.filter(flags, flag => flag.memory.roomName === room.name)

export const filterFlagsByRoomAndType = (flags, room, type): Flag[] =>
  _.filter(
    flags,
    flag => flag.memory.roomName === room.name && flag.memory.type === type
  )
