import { registerFN } from "src/screeps-profiler"

const _processObserver = (observer: StructureObserver) => {
  observer.observeRoom(observer.room.observeTarget)
}

export const processObserver: (StructureObserver) => void = registerFN(
  _processObserver,
  "observer.processObserver"
)
