import { getResources, getEnergyDeliveryTarget } from "src/creep-utils"

export const roleCourier = {
  run: function(creep: Creep) {
    const carryAmount = _.sum(creep.carry)
    if (creep.memory.storing === true && carryAmount < 100) {
      creep.memory.deliveryTarget = undefined
      creep.memory.deliveryType = undefined
      const pickupMinerals = creep.room.haveStorage || creep.room.haveTerminal
      getResources(creep, pickupMinerals)

      return true
    } else {
      creep.memory.pickupTarget = undefined
      creep.memory.withdrawTarget = undefined
      creep.memory.storing = carryAmount === 0
      if (creep.memory.storing) {
        return false
      }

      let target

      if (creep.memory.deliveryTarget) {
        target = Game.getObjectById(creep.memory.deliveryTarget)
        if (!target) {
          creep.memory.deliveryTarget = undefined
        }
      }

      let storedMinerals = _.without(_.keys(creep.carry), RESOURCE_ENERGY)
      if (_.isEmpty(storedMinerals)) {
        target = getEnergyDeliveryTarget(creep)
        creep.memory.deliveryTarget = target
        creep.memory.deliveryType = RESOURCE_ENERGY
      } else {
        creep.memory.deliveryType = _.first(storedMinerals)
        if (!(creep.room.haveStorage || creep.room.haveTerminal)) {
          creep.drop(creep.memory.deliveryType)
          return true
        }
        let lab = _.first(
          _.filter(
            creep.room.labs,
            lab =>
              lab.memory.mineralType == creep.memory.deliveryType &&
              (!lab.mineralType || lab.mineralType == lab.memory.mineralType) &&
              lab.mineralAmount < 1000
          )
        )
        if (lab) {
          target = lab
        } else if (
          creep.memory.deliveryType == RESOURCE_GHODIUM &&
          creep.room.nuker &&
          creep.room.nuker.ghodium < creep.room.nuker.ghodiumCapacity
        ) {
          target = creep.room.nuker
        }

        if (!target) {
          const terminal = Game.rooms[creep.memory.roomName].terminal
          const storage = Game.rooms[creep.memory.roomName].storage
          const haveTerminal = terminal && _.sum(terminal.store) < 295000

          const storedAmount =
            haveTerminal && (terminal.store[creep.memory.deliveryType] || 0)

          const storeRoomMineral =
            haveTerminal &&
            creep.memory.deliveryType == creep.room.memory.mineral &&
            storedAmount < 60000

          const storeMineral = haveTerminal && storedAmount < 30000
          const storageIsFull = _.sum(storage.store) == 1000000

          if (storeRoomMineral || storeMineral || storageIsFull) {
            target = terminal
          } else {
            target = storage
          }
        }
      }

      if (target) {
        let transferResult = creep.transfer(target, creep.memory.deliveryType)
        if (transferResult == ERR_NOT_IN_RANGE) {
          creep.moveTo(target, {
            visualizePathStyle: { stroke: "#ffffff" }
          })
          creep.memory.deliveryTarget = target.id
          return true
        }

        creep.memory.deliveryTarget = null
        if (transferResult == OK || transferResult == ERR_FULL) {
          return true
        }
        console.log("transferResult", transferResult)
        return true
      } else {
        return false
      }
    }
  }
}
