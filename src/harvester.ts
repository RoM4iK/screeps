import { registerFN } from "src/screeps-profiler"

import { ROLE_HARVESTER } from "src/constants"

const _processHarvester = (creep: Creep) => {
  if (creep.memory.inPosition) {
    const source: Source = Game.getObjectById(creep.memory.sourceId)
    const harvestResult = creep.harvest(source)
    if (creep.memory.harvestLinkId) {
      const harvestLink: StructureLink = Game.getObjectById(
        creep.memory.harvestLinkId
      )
      const transferResult = creep.transfer(harvestLink, RESOURCE_ENERGY)
      if (
        !_.includes(
          [OK, ERR_NOT_ENOUGH_RESOURCES, ERR_FULL, ERR_NOT_IN_RANGE],
          transferResult
        )
      ) {
        console.log(creep, creep.room, "transfer failed", transferResult)
      }
    }

    if (!_.includes([OK, ERR_INVALID_TARGET, ERR_BUSY], harvestResult)) {
      console.log(creep, creep.room, "harvest failed", harvestResult)
    }

    return
  }

  if (creep.memory.targetFlag) {
    const flag = Game.flags[creep.memory.targetFlag]
    if (!flag) {
      creep.memory.targetFlag = undefined
      return false
    }

    if (creep.pos.getRangeTo(flag) != 0) {
      creep.moveTo(flag.pos)
      creep.memory._moveStatic = false
      return true
    }

    let closestHarvestLink = creep.pos.findClosestByRange(
      creep.room.harvestLinks
    )
    creep.memory.sourceId = flag.memory.sourceId
    creep.memory.harvestLinkId = closestHarvestLink && closestHarvestLink.id
    creep.memory.inPosition = true
    creep.memory._moveStatic = true

    return
  }

  _.some(creep.room.harvestFlags, flag => {
    const haveMiner = _.some(
      _.values(creep.room.creeps),
      (miner: Creep) =>
        miner.memory.role == ROLE_HARVESTER &&
        miner.memory.targetFlag == flag.name
    )

    if (!haveMiner) {
      creep.memory.targetFlag = flag.name
      return true
    }
  })
}

export const processHarvester = registerFN(
  _processHarvester,
  "harvester.processHarvester"
)
