export const roleScout = creep => {
  creep.disableNotifications()
  if (creep.memory.scoutTarget) {
    let memory = Memory.rooms[creep.memory.scoutTarget]
    if (!memory) {
      Memory.rooms[creep.memory.scoutTarget] = memory = {}
    }
    if (memory.updatedAt && memory.updatedAt + 100 > Game.time) {
      creep.memory.scoutTarget = null
      return false
    }
    if (creep.memory.scoutTarget != creep.pos.roomName) {
      let targetPosition = new RoomPosition(25, 25, creep.memory.scoutTarget)
      creep.moveTo(targetPosition, {
        visualizePathStyle: { stroke: "#ffffff" }
      })
    }
    return true
  }

  if (creep.room.observeTarget) {
    if (
      Memory.rooms[creep.room.observeTarget].updatedAt ||
      0 + 100 < Game.time
    ) {
      creep.memory.scoutTarget = creep.room.observeTarget
      return false
    }
  }

  creep.moveTo(Game.rooms[creep.memory.roomName].spawns[0], {
    visualizePathStyle: { stroke: "#ffffff" }
  })
  return true
}
